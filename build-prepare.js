import * as fs from 'fs';
import { Nodir } from '@cyberjs.on/nodir';


const currentPath = new Nodir.Path(import.meta.url);

const pathWithoutProtocol = new Nodir.Path(currentPath.withoutUpperFloors(1));

const currentPathname = pathWithoutProtocol.withoutLowerFloors(1);

const targetPathname = `${currentPathname}/ts/src`;

const testsPathname = `${currentPathname}/ts/src/tests`;

const symlink = `${testsPathname}/requestIntercepting/web/src`;

if (fs.existsSync(symlink)) {
  console.log('Symlink of', targetPathname, 'already exists in', symlink, `\n`);
} else {
  /**
   * testar nalguma oportunidade se quando esse terceiro argumento estiver setado, em que pese ser exigido apenas para o rWindows, funciona normalmente nos
   * sistemas Unix. Caso não funcione, recorrer-se-á ao bom e velho "if" testando o código abaixo :'(
   */
  // console.log('plataform', process.platform, `\n`)
  await fs.promises.symlink(targetPathname, symlink, 'dir');
  console.log('Symlink of', targetPathname, 'has been created in', testsPathname, `\n`);
}
