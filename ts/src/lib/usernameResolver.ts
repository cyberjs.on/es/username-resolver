import { FileProtocolSource } from '@cyberjs.on/jazz-on';


export class UsernameResolver {

    #cops!: object[];

    constructor(jsonUrl: string) {
        this.#display(jsonUrl);
    }

    async #extractCopsFrom(url: string): Promise<object[]> {

        const fileSource = new FileProtocolSource(url);

        const data = await fileSource.read();

        return Promise.resolve(data) as any;
    }

    async #display(url: string) {

        await this.#extractCopsFrom(url).then(
            data => this.#cops = data
        );

        console.log(this.#cops)
    }

}
